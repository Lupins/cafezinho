#ifndef semantico_h
#define semantico_h
#include <iostream>
#include <string>
#include <map>
#include <stack>
#include <vector>
#include "tree.h"

using namespace std;

extern Toperador* Root;
extern "C" FILE *yyin;

map< string, stack < Toperador* > > symTable;
map< string, int >                  qntArgs;
stack< pair < string, int > >       pilha;

VarType retorna[2];

bool verificaSemantica(Toperador *no, int scopo);

string tipo[] = {
  "Raiz",
  "Bloco",
  "ListaVariaveis",
  "DeclaracaoVariavel",
  "Declaracao",
  "DeclaracaoVariavelArray",
  "ListaFuncoesVars",
  "ListaExpressao",
  "DeclaracaoFuncao",
  "DeclaracaoPrograma",
  "ParametroVariavel",
  "ParametroVariavelArray",
  "Variavel",
  "VariavelArray",
  "ListaComandos",
  "Comando",
  "Expressao",
  "Retorne",
  "Leia",
  "Escreva",
  "NovaLinha",
  "Se",
  "SeSenao",
  "Enquanto",
  "Atribuicao",
  "CondicaoTernaria",
  "E_",
  "Ou",
  "Igual",
  "Diferente",
  "Maior",
  "Menor",
  "MaiorIgual",
  "MenorIgual",
  "Adicao",
  "Subtracao",
  "Multiplicacao",
  "Divisao",
  "Modulo",
  "Negativo",
  "Negado",
  "ChamadaFuncao",
  "String",
  "Caracter",
  "Inteiro",
  "Vazio",
  "IdentificadorArray",
  "Identificador",
  "Unario",
  "Binario",
  "Ternario"
};

VarType verificaTipo(Toperador *no)
{
  cout << "Verifica tipo" << endl;
  string nome = no->id;

  switch(no->tipoOperador)
  {
    case Inteiro:
    case String:
    case Caracter:
    {
      return no->t_var;
    }
    break;

    case Unario:
    {
      verificaTipo(no->filhos[0]);
    }
    break;

    case Binario:
    {
      VarType v1 = verificaTipo(no->filhos[0]);
      VarType v2 = verificaTipo(no->filhos[1]);
      if(v1 != v2)
      {
        cout << "ERRO, comparacao deve ser feita com o mesmo tipo." << endl;
        exit(0);
      }
      return v2;
    }
    break;

    case Ternario:
    {
      VarType v1 = verificaTipo(no->filhos[0]);
      VarType v2 = verificaTipo(no->filhos[1]);
      VarType v3 = verificaTipo(no->filhos[2]);
      if(v2 != v3)
      {
        cout << "ERRO, comparacao deve ser feita com o mesmo tipo." << endl;
        exit(0);
      }
      if(v1 != INT_)
      {
        cout << "ERRO, operacoes condicionais devem ser do tipo inteiro." << endl;
        exit(0);
      }
      return v3;
    }
    break;

    case Atribuicao:
    {
      VarType v1 = verificaTipo(no->filhos[0]);
      VarType v2 = verificaTipo(no->filhos[1]);

      if(v1 != v2)
      {
        cout << "ERRO, atribuicao deve ser feita com o mesmo tipo." << endl;
        exit(0);
      }
      return v1;
    }
    break;

    case ChamadaFuncao:
    {
			if( !symTable.count(nome) ){
				cout << "ERRO, na linha [" << no->linha << "] funcao [" << nome << "] nao foi declarada" << endl;
				exit(0);
			}else if( symTable[nome].top()->tipoOperador != DeclaracaoFuncao )
      {
				cout << "ERRO, na linha [" << no->linha << "] ao declarar funcao [" << nome << "]" << endl;
				exit(0);
			}
			return symTable[nome].top()->t_var;
    }
    break;
  }
  return ERROR;
}

void verificaFilhos(Toperador* no, int scopo, int pos = 0)
{
  cout << "Verifica filhos" << endl;
  cout << "\nEscopo: " << no->scope << "\nN_Escopo: " << scopo << endl << "Tipo " << tipo[no->tipoOperador] << endl << endl;
  if(pos >= no->filhos.size())
    return;

  cout << "A? " << scopo + ((tipo[no->filhos[pos]->tipoOperador] == "Bloco") ? (1) : (0)) << endl;
  verificaSemantica(no->filhos[pos], scopo + ((tipo[no->filhos[pos]->tipoOperador] == "Bloco") ? (1) : (0)));

  if(no->filhos[pos]->tipoOperador == Bloco)
  {
    while( !pilha.empty() ){
		pair < string, int > at = pilha.top();
		if( symTable[at.first].empty() || at.second <= scopo ) break;
		pilha.pop();
		symTable[at.first].pop();
		if( symTable[at.first].empty() ) symTable.erase(at.first);
	}
  }
  verificaFilhos(no, scopo, pos + 1);
}

bool verificaSemantica(Toperador *no, int scopo)
{
  cout << "Verifica semantica" << endl;
  string nome = "";

  // if(no == NULL)
  //   cout << no->id << endl;
  if( no->id != "" )
    nome += no->id;
no->scope = scopo;
  cout << "Tipo operador: " << tipo[no->tipoOperador] << endl;
  cout << "ID: [" << no->id << "] -- " << no->scope << endl;


  switch(no->tipoOperador)
  {
    case Variavel:
    case VariavelArray:
    {
      cout << "1->" << endl;
      if(!symTable.count(nome))
      {
				stack<Toperador*>novo;
				symTable[nome] = novo;
			}
      pilha.push(make_pair(nome, scopo));

      if(!symTable[nome].empty() && symTable[nome].top()->scope == scopo)
      {
        cout << "* " << symTable[nome].top()->id << endl;
        cout << "ERRO, variavel: " << nome << " ja foi declarada, linha: " << no->linha << endl;
        exit(0);
      }else{
        symTable[nome].push(no);
      }
      cout << "1<-" << endl;
    }
    break;

    case Bloco:
    {
      cout << "2->" << endl;
      verificaFilhos(no, scopo);
      cout << "2<-" << endl;
    }
    break;

    case DeclaracaoFuncao:
    {
      cout << "3->" << endl;
      if(symTable.count(nome))
      {
        cout << "ERRO, funcao: " << nome << " ja foi declarada, linha: " << no->linha << endl;
        exit(0);
      }
      stack<Toperador*> novo;
      symTable[nome] = novo;
      symTable[nome].push(no);

      // cout << "# de filhos: " << no->filhos.size() << endl;

      if(no->filhos.size())
      {
        // cout << "Nome: " << nome << endl;
        qntArgs[nome] = no->filhos[0]->filhos.size();
        for(int i = 0 ; i < no->filhos[0]->filhos.size() ; i++)
          if(no->filhos[0]->filhos[i] != NULL){
            verificaSemantica(no->filhos[0]->filhos[i], scopo + 1);
          }
      }
      retorna[1] = no->t_var;
      retorna[0] = ERROR;

      // if(no->filhos[1] == NULL)
      //   cout << "Filho nulo" << endl;

      verificaSemantica(no->filhos[1], scopo + 1);

      while( !pilha.empty() ){
		pair < string, int > at = pilha.top();
		if( symTable[at.first].empty() || at.second <= scopo ) break;
		pilha.pop();
		symTable[at.first].pop();
		if( symTable[at.first].empty() ) symTable.erase(at.first);
	  }
      cout << "<-3" << endl;
    }
    break;

    case ChamadaFuncao:
    {
      cout << "4->" << endl;
      verificaTipo(no);
      verificaFilhos(no, scopo);

      if(no->filhos.size() != qntArgs[nome])
      {
        cout << "ERRO, quantidade de parametros da funcao: " << nome << " difere do esperado." << endl;
        exit(0);
      }
      cout << "<-4" << endl;
    }
    break;

    case Retorne:
    {
      cout << "5->" << endl;
      retorna[0] = INT_;
      if(retorna[1] != ERROR)
      {
        VarType var_type = verificaTipo(no->filhos[0]);
        if(var_type != retorna[1])
        {
          cout << "ERRO, tipo de retorno da funcao: " << nome << " difere do esperado." << endl;
          exit(0);
        }
      }else{
        cout << "ERRO, funcao: " << nome << " sem retorno." << endl;
        exit(0);
      }
      cout << "<-5" << endl;
    }
    break;

    case SeSenao:
    case Se:
    case Enquanto:
    {
      cout << "6->" << endl;
      if(verificaTipo(no->filhos[0]) != INT_)
      {
        cout << "ERRO, operacoes condicionais devem ser do tipo inteiro." << endl;
        exit(0);
      }

      verificaFilhos(no, scopo + 1);

      while( !pilha.empty() ){
		pair < string, int > at = pilha.top();
		if( symTable[at.first].empty() || at.second <= scopo ) break;
		pilha.pop();
		symTable[at.first].pop();
		if( symTable[at.first].empty() ) symTable.erase(at.first);
	  }
      cout << "<-6" << endl;
    }
    break;

    case Atribuicao:
    case Unario:
    case Binario:
    case CondicaoTernaria:
    {
      cout << "7->" << endl;
      verificaTipo(no);
      verificaFilhos(no, scopo);
      cout << "7<-" << endl;
    }
    break;

    case Escreva:
    case Caracter:
    case ListaComandos:
    case ListaVariaveis:
    case Expressao:
    case Leia:
    case Inteiro:
    case ListaExpressao:
    {
      cout << "8->" << endl;
      verificaFilhos(no, scopo);
      cout << "8<-" << endl;
    }
    break;
  }
  return 0;
}

#endif
