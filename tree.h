#ifndef TREE_H
#define TREE_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

typedef enum
{
  Raiz,
  Bloco,
  ListaVariaveis,
  DeclaracaoVariavel,
  Declaracao,
  DeclaracaoVariavelArray,
  ListaFuncoesVars,
  ListaExpressao,
  DeclaracaoFuncao,
  DeclaracaoPrograma,
  ParametroVariavel,
  ParametroVariavelArray,
  Variavel,
  VariavelArray,
  ListaComandos,
  Comando,
  Expressao,
  Retorne,
  Leia,
  Escreva,
  NovaLinha,
  Se,
  SeSenao,
  Enquanto,
  Atribuicao,
  CondicaoTernaria,
  E_,
  Ou,
  Igual,
  Diferente,
  Maior,
  Menor,
  MaiorIgual,
  MenorIgual,
  Adicao,
  Subtracao,
  Multiplicacao,
  Divisao,
  Modulo,
  Negativo,
  Negado,
  ChamadaFuncao,
  String,
  Caracter,
  Inteiro,
  Vazio,
  IdentificadorArray,
  Identificador,
  Unario,
  Binario,
  Ternario
} TespecieOperador;

typedef enum
{
  INT_,
  CHAR_,
  ERROR,
  VAZIO_
} VarType;

struct operador;

typedef struct operador
{
  int               scope;
  int               linha;
  int               ArraySize;
  int               IntVal;
  char              CharVal;
  string            id;
  TespecieOperador  tipoOperador;
  VarType           t_var;
  vector<operador*> filhos;

  operador( )
  {
  	scope        = 0;
  	linha        = 0;
  	ArraySize    = 0;
  	IntVal       = 0;
  	CharVal      = 0;
  	id           = "";
  	tipoOperador = Vazio;
  	t_var        = VAZIO_;
  }
} Toperador;
#endif
