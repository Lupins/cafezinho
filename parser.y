// Linux somente
/*%code requires{
    #include "tree.h"
}*/
%{

// Linux somente
//#include <bits/stdc++.h>

#include "tree.h"
#include "semantic.h"


using namespace std;

extern "C"  int yydebug;
extern      int yylex();
extern "C"  int yyparse();
extern "C"  int yylineno;
extern "C"  char *yytext;
extern "C"  FILE *yyin;
void yyerror(const char *str);
string getID(char *s);

Toperador *Root;

%}

%union {
    int       IntVal;
    char      CharVal;
    char      *StrVal;
    char      *lexema;
    VarType   t_var;
    Toperador *Tpont;
}

%start Programa


%token PROGRAMA
%token <CharVal>CAR
%token <IntVal>INT
%token RETORNE
%token LEIA
%token ESCREVA
%token NOVALINHA
%token SE
%token ENTAO
%token SENAO
%token ENQUANTO
%token EXECUTE
%token OU
%token E
%token IGUAL
%token DIFERENTE
%token MENOR
%token MAIOR
%token MAIORIGUAL
%token MENORIGUAL
%token MAIS
%token MENOS
%token VEZES
%token DIVIDE
%token MOD
%token NAO
%token RECEBE
%token CARCONST
%token <StrVal>ID
%token <IntVal>INTCONST
%token <StrVal>CADEIACAR
%token ABREP
%token FECHAP
%token VIRGULA
%token PONTOEV
%token ABREC
%token FECHAC
%token ABRECH
%token FECHACH
%token DOISP
%token INTER

%type<t_var> Tipo
%type<Tpont> Programa
%type<Tpont> DeclFuncVar
%type<Tpont> DeclProg
%type<Tpont> DeclVar
%type<Tpont> DeclFunc
%type<Tpont> ListaParametros
%type<Tpont> ListaParametrosCont
%type<Tpont> Bloco
%type<Tpont> ListaDeclVar
%type<Tpont> ListaComando
%type<Tpont> Comando
%type<Tpont> Expr
%type<Tpont> AssignExpr
%type<Tpont> CondExpr
%type<Tpont> OrExpr
%type<Tpont> AndExpr
%type<Tpont> EqExpr
%type<Tpont> DesigExpr
%type<Tpont> AddExpr
%type<Tpont> MulExpr
%type<Tpont> UnExpr
%type<Tpont> LValueExpr
%type<Tpont> PrimExpr
%type<Tpont> ListExpr

%error-verbose

%%

Programa:
  DeclFuncVar DeclProg
  { cout << "\nRegra: 1\n" << endl;
    Root = new operador();
    Root->id = "programa";
    Root->linha = yylineno;
    Root->tipoOperador = Bloco;
    Root->filhos.push_back($1);
    Root->filhos.push_back($2);
  };

DeclFuncVar:
  Tipo ID DeclVar PONTOEV DeclFuncVar
  { cout << "\nRegra: 2\n" << endl;
    for(int i = 0; i < $3->filhos.size(); i++)
        $3->filhos[i]->t_var = $1;

    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Variavel;
    aux->t_var = $1;
    aux->id = getID($2);

    $3->filhos.push_back(aux);

    $$ = $5;
    $$->filhos.push_back($3);
  } |
  Tipo ID ABREC INTCONST FECHAC DeclVar PONTOEV DeclFuncVar
  { cout << "\nRegra: 3\n" << endl;
    for(int i = 0; i < $6->filhos.size(); i++)
        $6->filhos[i]->t_var = $1;

    Toperador *aux = new operador();
    aux->t_var = $1;
    aux->linha = yylineno;
    aux->tipoOperador = VariavelArray;
    aux->id = getID($2);

    $6->filhos.push_back(aux);

    $$ = $8;
    $$->filhos.push_back($6);
  } |
  Tipo ID DeclFunc DeclFuncVar
  { cout << "\nRegra: 4\n" << endl;
    $3->id = getID($2);
    $3->t_var = $1;

    $4->filhos.push_back($3);

    $$ = $4;
  } |
  { cout << "\nRegra: 5\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ListaVariaveis;
  };

DeclProg:
  PROGRAMA Bloco
  { cout << "\nRegra: 6\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Vazio;

    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = DeclaracaoFuncao;
    $$->filhos.push_back( aux );
    $$->filhos.push_back( $2 );
    $$->t_var = VAZIO_;
  };

DeclVar:
  VIRGULA ID DeclVar
  { cout << "\nRegra: 7\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Variavel;
    aux->id = getID($2);

    $$ = $3;
    $$->filhos.push_back( aux );
  } |
  VIRGULA ID ABREC INTCONST FECHAC DeclVar
  { cout << "\nRegra: 8\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = VariavelArray;
    aux->id = getID($2);

    $$ = $6;

    $$->filhos.push_back( aux );
  } |
  { cout << "\nRegra: 9\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ListaVariaveis;
  };

DeclFunc:
  ABREP ListaParametros FECHAP Bloco
  { cout << "\nRegra: 10\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = DeclaracaoFuncao;
    $$->filhos.push_back($2);
    $$->filhos.push_back($4);
  };

ListaParametros:
  ListaParametrosCont
  { cout << "\nRegra: 11\n" << endl;
    $$ = $1;
  } |
  { cout << "\nRegra: 12\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Vazio;
  };

ListaParametrosCont:
  Tipo ID
  { cout << "\nRegra: 13\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Variavel;
    aux->t_var = $1;
    aux->id = getID($2);

    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ListaVariaveis;
    $$->filhos.push_back(aux);
  } |
  Tipo ID ABREC FECHAC
  { cout << "\nRegra: 14\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = VariavelArray;
    aux->t_var = $1;
    aux->id = getID($2);

    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ListaVariaveis;
    $$->filhos.push_back( aux );
  } |
  Tipo ID VIRGULA ListaParametrosCont
  { cout << "\nRegra: 15\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Variavel;
    aux->t_var = $1;
    aux->id = getID($2);

    $4->filhos.push_back( aux );

    $$ = $4;
  } |
  Tipo ID ABREC FECHAC VIRGULA ListaParametrosCont
  { cout << "\nRegra: 16\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = VariavelArray;
    aux->t_var = $1;
    aux->id = getID($2);

    $6->filhos.push_back( aux );
    $$ = $6;
  };

Bloco:
  ABRECH ListaDeclVar ListaComando FECHACH
  { cout << "\nRegra: 17\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Bloco;
    $$->filhos.push_back( $2 );
    $$->filhos.push_back( $3 );
  } |
  ABRECH ListaDeclVar FECHACH
  { cout << "\nRegra: 18\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Bloco;
    $$->filhos.push_back( $2 );
  };

ListaDeclVar:
  Tipo ID DeclVar PONTOEV ListaDeclVar
  { cout << "\nRegra: 19\n" << endl;
    for(int i = 0; i < $3->filhos.size(); i++)
         $3->filhos[i]->t_var = $1;

    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = Variavel;
    aux->t_var = $1;
    aux->id = getID($2);

    $3->filhos.push_back( aux );

    $$ = $5;
    $$->filhos.push_back( $3 );
  } |
  Tipo ID  ABREC INTCONST FECHAC  DeclVar  PONTOEV  ListaDeclVar
  { cout << "\nRegra: 20\n" << endl;
    for(int i = 0; i < $6->filhos.size(); i++)
      $6->filhos[i]->t_var = $1;

    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = VariavelArray;
    aux->t_var = $1;
    aux->id = getID($2);

    $6->filhos.push_back( aux );

    $$ = $8;
    $$->filhos.push_back( $6 );
  } |
  { cout << "\nRegra: 21\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ListaVariaveis;
  };

Tipo:
  INT
  { cout << "\nRegra: 22\n" << endl;
    $$ = INT_;
  } |
  CAR
  { cout << "\nRegra: 23\n" << endl;
    $$ = CHAR_;
  };

ListaComando:
  ListaDeclVar ListaComando
  { cout << "\nRegra: 24\n" << endl;
    $$ = $2;
    $$->filhos.push_back( $1 );
  } |
  Comando
  { cout << "\nRegra: 25\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Declaracao;
    $$->filhos.push_back( $1 );
  } |
  Comando ListaComando
  { cout << "\nRegra: 26\n" << endl;
    $$ = $2;
    $$->filhos.push_back( $1 );
  };

Comando:
  PONTOEV
  { cout << "\nRegra: 27\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Declaracao;
  } |
  Expr PONTOEV
  { cout << "\nRegra: 28\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Expressao;
    $$->filhos.push_back( $1 );
  } |
  RETORNE Expr PONTOEV
  { cout << "\nRegra: 29\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Retorne;
    $$->filhos.push_back( $2 );
  } |
  LEIA LValueExpr PONTOEV
  { cout << "\nRegra: 30\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Leia;
    $$->filhos.push_back( $2 );
  } |
  ESCREVA CADEIACAR PONTOEV
  { cout << "\nRegra: 31\n" << endl;
    Toperador *aux = new operador();
    aux->linha = yylineno;
    aux->tipoOperador = String;
    aux->id = getID($2);

    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Escreva;
    $$->t_var = CHAR_;
    $$->filhos.push_back( aux );
  } |
  ESCREVA Expr PONTOEV
  { cout << "\nRegra: 32\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Escreva;
    $$->t_var = INT_;
    $$->filhos.push_back( $2 );
  } |
  NOVALINHA PONTOEV
  { cout << "\nRegra: 33\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = NovaLinha;
  } |
  SE ABREP Expr FECHAP ENTAO Comando
  { cout << "\nRegra: 34\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Se;
    $$->filhos.push_back( $3 );
    $$->filhos.push_back( $6 );
  } |
  SE ABREP Expr FECHAP ENTAO Comando SENAO Comando
  { cout << "\nRegra: 35\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = SeSenao;
    $$->filhos.push_back($3);
    $$->filhos.push_back($6);
    $$->filhos.push_back($8);
  } |
  ENQUANTO ABREP Expr FECHAP EXECUTE Comando
  { cout << "\nRegra: 36\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Enquanto;
    $$->filhos.push_back( $3 );
    $$->filhos.push_back( $6 );
  } |
  Bloco
  { cout << "\nRegra: 37\n" << endl;
    $$ = $1;
  };

Expr:
  AssignExpr
  { cout << "\nRegra: 38\n" << endl;
    $$ = $1;
  };

AssignExpr:
  CondExpr
  { cout << "\nRegra: 39\n" << endl;
    $$ = $1;
  } |
  LValueExpr RECEBE AssignExpr
  { cout << "\nRegra: 40\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Atribuicao;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  };

CondExpr:
  OrExpr
  { cout << "\nRegra: 41\n" << endl;
    $$ = $1;
  } |
  OrExpr INTER Expr DOISP CondExpr
  { cout << "\nRegra: 42\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Ternario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
    $$->filhos.push_back( $5 );
  };

OrExpr:
  OrExpr OU AndExpr
  { cout << "\nRegra: 43\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  AndExpr
  { cout << "\nRegra: 44\n" << endl;
    $$ = $1;
  };

AndExpr:
  AndExpr E EqExpr
  { cout << "\nRegra: 45\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  EqExpr
  { cout << "\nRegra: 46\n" << endl;
    $$ = $1;
  };

EqExpr:
  EqExpr IGUAL DesigExpr
  { cout << "\nRegra: 47\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  EqExpr DIFERENTE DesigExpr
  { cout << "\nRegra: 48\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  DesigExpr
  { cout << "\nRegra: 49\n" << endl;
    $$ = $1;
  };

DesigExpr:
  DesigExpr MENOR AddExpr
  { cout << "\nRegra: 50\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  DesigExpr MAIOR AddExpr
  { cout << "\nRegra: 51\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  DesigExpr MAIORIGUAL AddExpr
  { cout << "\nRegra: 52\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  DesigExpr MENORIGUAL AddExpr
  { cout << "\nRegra: 53\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  AddExpr
  { cout << "\nRegra: 54\n" << endl;
    $$ = $1;
  };

AddExpr:
  AddExpr MAIS MulExpr
  { cout << "\nRegra: 55\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  AddExpr MENOS MulExpr
  { cout << "\nRegra: 56\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  MulExpr
  { cout << "\nRegra: 57\n" << endl;
    $$ = $1;
  };

MulExpr:
  MulExpr VEZES UnExpr
  { cout << "\nRegra: 58\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  MulExpr DIVIDE UnExpr
  { cout << "\nRegra: 59\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  MulExpr MOD UnExpr
  { cout << "\nRegra: 60\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Binario;
    $$->filhos.push_back( $1 );
    $$->filhos.push_back( $3 );
  } |
  UnExpr
  { cout << "\nRegra: 61\n" << endl;
    $$ = $1;
  };

UnExpr:
  MENOS PrimExpr
  { cout << "\nRegra: 62\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Unario;
    $$->filhos.push_back( $2 );
  } |
  NAO PrimExpr
  { cout << "\nRegra: 63\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Unario;
    $$->filhos.push_back( $2 );
  } |
  PrimExpr
  { cout << "\nRegra: 64\n" << endl;
    $$ = $1;
  };

LValueExpr:
  ID ABREC Expr FECHAC
  { cout << "\nRegra: 65\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;;
    $$->tipoOperador = IdentificadorArray;
    $$->id = getID($1);
    $$->filhos.push_back( $3 );
  } |
  ID
  { cout << "\nRegra: 66\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Identificador;
    // cout << "Entrou aqui" << endl << endl;
    $$->id = getID($1);
    // cout << "Saiu aqui" << endl << endl;
  };

PrimExpr:
  ID ABREP ListExpr FECHAP
  { cout << "\nRegra: 67\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ChamadaFuncao;
    $$->filhos.push_back( $3 );
    $$->id = getID($1);
  } |
  ID ABREP FECHAP
  { cout << "\nRegra: 68\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = ChamadaFuncao;
    $$->id = getID($1);
  } |
  ID ABREC Expr FECHAC
  { cout << "\nRegra: 69\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = IdentificadorArray;
    $$->id = getID($1);
    $$->filhos.push_back( $3 );
  } |
  ID
  { cout << "\nRegra: 70\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Identificador;
    $$->id = getID($1);
  } |
  CARCONST
  { cout << "\nRegra: 71\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Caracter;
    $$->t_var = CHAR_;
  } |
  INTCONST
  { cout << "\nRegra: 72\n" << endl;
    $$ = new operador();
    $$->linha = yylineno;
    $$->tipoOperador = Inteiro;
    $$->t_var = INT_;
  } |
  ABREP Expr FECHAP
  { cout << "\nRegra: 73\n" << endl;
    $$ = $2;
  };

ListExpr:
    AssignExpr
    { cout << "\nRegra: 74\n" << endl;
      $$ = new operador();
      $$->linha = yylineno;
      $$->tipoOperador = ListaExpressao;
      $$->filhos.push_back( $1 );

    } |
    ListExpr  VIRGULA  AssignExpr
    { cout << "\nRegra: 75\n" << endl;
      $$ = $1;
      $$->filhos.push_back( $3 );
    };
%%

void yyerror(const char* errmsg)
{
  printf("\nERRO: %s\n", errmsg);
}

int yywrap(void)
{
  return 1;
}

string getID(char *s)
{
  if(s == NULL)
    return "";
  char str[100];
  strcpy(str, s);
  return str;
}

int main(int argc, char** argv)
{
  if(argc == 2)
  {
    yyin = fopen(argv[1], "r");
    if(!yyin)
    {
      printf("Arquivo invalido.\n");
      exit(1);
    }
  }

  #if YYDEBUG == 1
  extern int yydebug;
  yydebug = 1;
  #endif

  cout << "\n\tSintatic init\n" << endl;
  yyparse();
  cout << "\n\tSUCESSO SINTATICO!!!\n" << endl;

  cout << "\n\tSemantic init\n" << endl;
  cout << "\nRaiz: \n\tScope: " << Root->scope << "\n\tLinha: " << Root->linha << "\n\tArraySize: " << Root->ArraySize << "\n\tIntVal: "<< Root->IntVal << "\n\tCharVal: " << Root->CharVal << "\n\tID: " << Root->id << "\n\tTipoOperador: " << Root->tipoOperador << "\n\tT_Var: " << Root->t_var << endl << endl;
  verificaSemantica(Root, 0);
  cout << "\n\tSUCESSO SEMANTICO!!!\n" << endl;

  return 0;
}
