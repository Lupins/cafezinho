# README #

Mini compiler of a simple language called Cafezinho.

### How do I get set up? ###

#### What you'll need:
* Lex
* Bison

### How to execute it? ###

#### On OSX:
make clean
make all
make test

#### On Linux other than OSX:
In the parser.y remove the comments of the following lines: 2, 3, 4 and 8

#### On Windows
I won't even...

### Who do I talk to? ###

* Guilherme Leite - guilherme.vieira.leite@gmail.com