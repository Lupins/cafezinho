%{
#include <stdlib.h>
#include <strings.h>
#include "tree.h"
#include "parser.tab.h"

int line = 1;
extern int currentSymTabSize;
%}

/* Regular Expressions */

whitespace 				[ \t]([ \t])*
newline						(\n|\r|\r\n)
id 								[A-Za-z]([A-Za-z]|[0-9])*
intconst 					[0-9]([0-9])*
quote 						\"|\'
punctuation				[:;]
phrase						([A-Za-z]|[0-9]|{whitespace}|{punctuation})*
new_line_prahse		{phrase}*{newline}{phrase}*
invalid_carconst 	{quote}({new_line_prahse})*{quote}
carconst 					{quote}{phrase}[^{}]{quote}
comment						"/*"({intconst}|{whitespace}|[A-Za-z]|{newline}|{punctuation})*"*/"

%option noyywrap

%%

{newline}           {++line;}
{comment}           { }
{whitespace}        { }
programa            {return(PROGRAMA);}
car                 {return(CAR);}
int                 {return(INT);}
retorne             {return(RETORNE);}
leia                {return(LEIA);}
escreva             {return(ESCREVA);}
novalinha           {return(NOVALINHA);}
se                  {return(SE);}
entao               {return(ENTAO);}
senao               {return(SENAO);}
enquanto            {return(ENQUANTO);}
execute             {return(EXECUTE);}
ou                  {return(OU);}
e                   {return(E);}
"=="                {return(IGUAL);}
"!="                {return(DIFERENTE);}
"<"                 {return(MENOR);}
">"                 {return(MAIOR);}
">="                {return(MAIORIGUAL);}
"<="                {return(MENORIGUAL);}
"+"                 {return(MAIS);}
"-"                 {return(MENOS);}
"*"                 {return(VEZES);}
"/"                 {return(DIVIDE);}
"%"                 {return(MOD);}
"!"                 {return(NAO);}
"="                 {return(RECEBE);}
"("                 {return(ABREP);}
")"                 {return(FECHAP);}
","                 {return(VIRGULA);}
";"                 {return(PONTOEV);}
"["                 {return(ABREC);}
"]"                 {return(FECHAC);}
"{"                 {return(ABRECH);}
"}"                 {return(FECHACH);}
":"                 {return(DOISP);}
"?"                 { return INTER; }
{id}                { yylval.lexema = (char*)malloc(sizeof(strlen(yytext) + 1));
                      if(yylval.lexema)
                      strcpy(yylval.lexema, yytext);
                      return ID;
                    }
{intconst}					{ return INTCONST; }
{invalid_carconst} 	{	printf("ERRO: MLS na linha %d\n", line); exit(-1); }
{carconst}					{ return CARCONST; }
.										{ printf( "--%s-- ERRO: CARACTERE INVALIDO, perto da LINHA: %d\n", yytext, line); exit(-1); }

%%
//
// int main( int argc, char **argv )
// {
// 	char token;
// 	yyin = fopen(argv[1], "r");
// 	while ((token = yylex()));
//   fclose(yyin);
// }
