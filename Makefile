PHONY: clean test

all: yacc lex
	g++ -o cafezinho lex.yy.c parser.tab.c

all_old: lex yacc
	gcc -o cafezinho lex.c parser.c

lex:
	flex lex.l

lex_old:
	flex -o lex.c lex.l

yacc:
	bison -d parser.y --debug --verbose

yacc_old:
	bison -d -o parser.c parser.y

test:
	# sintatico

	# Códigos corretos
	./cafezinho ./testes/sintatico/expressao1Correto.txt
	./cafezinho ./testes/sintatico/fatorial.txt
	# Comentário possui 'é', não consigo adicionar ele na expressão regular.
	# ./cafezinho ./testes/sintatico/fatorialCorreto.txt
	./cafezinho ./testes/sintatico/selectionSortCorreto.txt

	# Códigos com erros
	# ./cafezinho ./testes/sintatico/erroLin6AsteriscoAmais.txt
	# ./cafezinho ./testes/sintatico/expressao1ErroLin4CadeiaNaoTermina.txt
	# ./cafezinho ./testes/sintatico/expressao1ErroLin4PontoVirg.txt
	# ./cafezinho ./testes/sintatico/fatorialErroLin1Comentario.txt
	# ./cafezinho ./testes/sintatico/fatorialErroLin15String.txt
	# ./cafezinho ./testes/sintatico/selectionSortErroLin19FechachavesEsperado.txt

	# semantico

	# Códigos corretos
	./cafezinho ./testes/semantico/expressao1Correto.txt
	./cafezinho ./testes/semantico/fatorialCorreto.txt
	./cafezinho ./testes/semantico/selectionSortCorreto.txt

	# Códigos com erros
	# ./cafezinho ./testes/semantico/fatorialErroLin3NomeDeclaradoNoMesmoEscopo.txt
	# ./cafezinho ./testes/semantico/fatorialErroLin4TipoRetornado.txt
	# ./cafezinho ./testes/semantico/fatorialErroLin5TipoRetornado.txt
	# ./cafezinho ./testes/semantico/selectionSortErro34VariavelNaoDeclarada.txt
	# ./cafezinho ./testes/semantico/selectionSortErroLin8VariavelNaoDeclarada.txt
	# ./cafezinho ./testes/semantico/selectionSortErroLin33NumparamDistintos.txt
	# ./cafezinho ./testes/semantico/selectionSortErroLin34ParametrosTiposDistintos.txt

clean:
	clear
	rm -f lex.yy.c parser.c parser.h lex.c lex parser.tab.c parser.tab.h
